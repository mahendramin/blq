fun main() {
    val timeIn24HFormat = "03:40:44 PM"
    val hour = "${timeIn24HFormat[0]}${timeIn24HFormat[1]}".toInt()
    val timeIn12HFormat =
        timeIn24HFormat.replaceRange(0, 2, "${hour + 12}").replaceRange(8..timeIn24HFormat.lastIndex, "")
    print(timeIn12HFormat)
}