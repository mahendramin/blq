fun main() {
    print("n: ")
    val n = readln().toInt()
    generateFibonacci(n)
}

private fun generateFibonacci(n: Int) {
    val fibonacciNumber: MutableList<Int> = ArrayList()
    fibonacciNumber.add(1)
    fibonacciNumber.add(1)

    for (i in 2 until n) {
        fibonacciNumber.add(fibonacciNumber[i - 1] + fibonacciNumber[i - 2])
    }

    for (i in 0 until n) {
        print("${fibonacciNumber[i]} ")
    }
}