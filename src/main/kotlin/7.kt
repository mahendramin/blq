fun main() {
    //var intArray = arrayOf<Int>()
//    var intArray = arrayOf(8, 7, 0, 2, 7, 1, 7, 6, 3, 0, 7, 1, 3, 4, 6, 1, 6, 4, 3)
//
//    var conditioner = true
//
//    var iteration = 0


//    while (conditioner) {
//        print("isi nilai array ke-$iteration: ")
//        val numArray = readln().toInt()
//        intArray += numArray
//
//        print("tambah nilai lagi(y/n): ")
//        val input = readln()
//
//        if (input != "y") {
//            conditioner = false
//        }
//        iteration++
//    }


    print("input (pisahkan dengan spasi): ")
    val input = readln().trim().split(" ")
    val intArray = mutableListOf<Int>()
    input.forEach {
        intArray.add(it.toInt())
    }

    val mean: Double

    //mean (rata-rata)
    var sum = 0
    for (i in 0 until intArray.size) {
        sum += intArray[i]
    }
    mean = sum.toDouble() / intArray.size

    println("mean: $mean")

    //median
    for (k in 0 until intArray.size) {
        for (j in 0 until intArray.size - 1) {
            if (intArray[j] > intArray[j + 1]) {
                val temp = intArray[j + 1]
                intArray[j + 1] = intArray[j]
                intArray[j] = temp
            }
        }
    }

    val median: Double = if (intArray.size % 2 == 1) {
        intArray[intArray.size / 2].toDouble()
    } else {
        val firstMedian = intArray[(intArray.size / 2) - 1]
        val secondMedian = intArray[intArray.size / 2]
        ((firstMedian + secondMedian).toDouble() / 2)
    }
    println("median: $median")

    //modus
    var currentCount = 0
    var max = 0
    var maxValue = 0
    var angka = intArray[0]

    for (n in 0 until intArray.size) { //1112234
        if (intArray[n] == angka) {
            currentCount += 1
        } else {
            if (currentCount > max) {
                maxValue = angka
                max = currentCount
            }
            angka = intArray[n]

            currentCount = 1
        }
    }

    println("modus: angka $maxValue dengan $max kali kemunculan")
}