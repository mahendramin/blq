fun main() {
    print("input: ")
    val input = readln().trim().split(" ")
    val listInput = mutableListOf<Int>()
    input.forEach {
        listInput.add(it.toInt())
    }
    println(listInput.joinToString())
    for (i in listInput.indices) {
        for (j in 0 until listInput.size - 1) {
            if (listInput[j] > listInput[j + 1]) {
                val temp = listInput[j + 1]
                listInput[j + 1] = listInput[j]
                listInput[j] = temp
            }
        }
    }

    println(listInput.joinToString())

}