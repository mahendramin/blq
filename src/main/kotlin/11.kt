fun main() {
    print("input: ")
    val sentence = readln().trim().reversed()
    sentence.forEach {
        println("***$it***")
    }
}