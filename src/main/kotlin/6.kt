fun main() {
    print("input kata: ")
    val word = readln().trim()
    var isPalindrome = true
    var startIndex = 0
    var endIndex = word.length - 1
    while (startIndex < endIndex) {
        if (word[startIndex] != word[endIndex]) {
            isPalindrome = false
            break
        }
        startIndex++
        endIndex--
    }
    print(isPalindrome)
}