fun main() {
    print("input: ")
    val sentence = readln().trim().split(" ")
    val maskedSentence = sentence.map { string ->
        val firstChar = string[0]
        val lastChar = string[string.lastIndex]
        val maskedChar = "***"
        firstChar + maskedChar + lastChar
    }
    print(maskedSentence.joinToString(" "))
}