fun main() {
    print("list: ")
//    val listNumInString = readln().trim().split(" ")
//    val listNum = listNumInString.map {
//        it.toInt()
//    }
    val listNum = listOf(3, 3, 9, 6, 7, 8, 23)
    val listFibonacci = generateFibonacci(listNum.size)
    var min = 0
    val listIndex = mutableListOf<Int>()
    println(listNum.joinToString())
    println(listFibonacci.joinToString())
    var selisih: Int
    for (i in listNum.indices) {
        selisih = listNum[i] - listFibonacci[i]
        if (selisih <= min) {
            min = selisih
            listIndex.add(i)
        }
    }
    listIndex.forEach {
        println("meleleh: ${listNum[it]}")
    }
}

private fun generateFibonacci(n: Int): List<Int> {
    val fibonacciNumber: MutableList<Int> = ArrayList()
    fibonacciNumber.add(1)
    fibonacciNumber.add(1)

    for (i in 2 until n) {
        fibonacciNumber.add(fibonacciNumber[i - 1] + fibonacciNumber[i - 2])
    }
    return fibonacciNumber
}