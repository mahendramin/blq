fun main() {
    print("input: ")
    val catatanPerjalanan = readln()
        .uppercase()
        .trim()
        .chunked(1)
        .toTypedArray()

    for (i in catatanPerjalanan.indices) {
        if (catatanPerjalanan[i] != NAIK && catatanPerjalanan[i] != TURUN) {
            println("Invalid input")
            return
        }
    }

    var position = 0
    var pointer = ""

    var gunungQty = 0
    var lembahQty = 0

    for (i in catatanPerjalanan.indices) {

        if (position == 0) {
            pointer = catatanPerjalanan[i]
        }

        when (catatanPerjalanan[i]) {
            NAIK -> position += 1
            TURUN -> position -= 1
        }

        if (position == 0) {
            when (pointer) {
                NAIK -> gunungQty += 1
                TURUN -> lembahQty += 1
            }
        }
    }

    println(
        """
        Gunung: $gunungQty
        Lembah: $lembahQty 
    """.trimIndent()
    )
}

const val NAIK = "N"
const val TURUN = "T"
