fun main() {
    print("jumlah makanan: ")
    val foodQty = readln().toInt()
    //harga, apakah mengandung ikan
    val foodList = mutableListOf<Pair<Int, Boolean>>()
    for (i in 0 until foodQty) {
        print("nama menu ${i + 1}: ")
        val menu = readln()
        print("harga: ")
        val price = readln().toInt()
        print("apakah mengandung ikan (true/false): ")
        val isContainingFish = readln().toBoolean()
        foodList.add(Pair(price, isContainingFish))
    }

    var saya = 0.0
    var teman1 = 0.0
    var teman2 = 0.0
    var teman3 = 0.0

    foodList.forEach {
        if (it.second) {
            saya += (it.first + (it.first * 0.1) + (it.first * 0.05)) / 3
            teman1 += (it.first + (it.first * 0.1) + (it.first * 0.05)) / 3
            teman2 += (it.first + (it.first * 0.1) + (it.first * 0.05)) / 3
        } else {
            saya += (it.first + (it.first * 0.1) + (it.first * 0.05)) / 4
            teman1 += (it.first + (it.first * 0.1) + (it.first * 0.05)) / 4
            teman2 += (it.first + (it.first * 0.1) + (it.first * 0.05)) / 4
            teman3 += (it.first + (it.first * 0.1) + (it.first * 0.05)) / 4
        }
    }

    print(
        """
        saya: $saya
        teman 1: $teman1
        teman 2: $teman2
        teman 3 (alergi ikan): $teman3
    """.trimIndent()
    )
}
