import java.time.Duration
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

fun main() {
    println("format input: dd MMMM yyyy (contoh: 05 Januari 2019) ")
    print("tanggal pinjam: ")
    val tanggalPinjam = readln()

    print("tanggal pengembalian: ")
    val tanggalPengembalian = readln()

    print("Apakah pinjam buku A (true/false): ")
    val isPinjamBukuA = readln().toBoolean()
    val batasPinjamADalamHari = 14

    print("Apakah pinjam buku B (true/false): ")
    val isPinjamBukuB = readln().toBoolean()
    val batasPinjamBDalamHari = 3

    print("Apakah pinjam buku C (true/false): ")
    val isPinjamBukuC = readln().toBoolean()

    print("Apakah pinjam buku D (true/false): ")
    val isPinjamBukuD = readln().toBoolean()

    val batasPinjamCDanDDalamHari = 7

    val biayaDendaPerHari = 100

    val localeIndo = Locale("id", "ID")
    val dateTimeFormatter = DateTimeFormatter.ofPattern("dd MMMM yyyy", localeIndo)

    val dateTanggalPinjam = LocalDate.parse(tanggalPinjam, dateTimeFormatter)
    val dateTanggalPengembalian = LocalDate.parse(tanggalPengembalian, dateTimeFormatter)

    val durasiPinjam =
        Duration.between(dateTanggalPinjam.atStartOfDay(), dateTanggalPengembalian.atStartOfDay()).toDays().toInt()
    var totalDenda = 0

    if (isPinjamBukuA) {
        totalDenda += if (durasiPinjam > batasPinjamADalamHari) durasiPinjam * biayaDendaPerHari else 0
    }
    if (isPinjamBukuB) {
        totalDenda += if (durasiPinjam > batasPinjamBDalamHari) durasiPinjam * biayaDendaPerHari else 0
    }
    if (isPinjamBukuC) {
        totalDenda += if (durasiPinjam > batasPinjamCDanDDalamHari) durasiPinjam * biayaDendaPerHari else 0
    }
    if (isPinjamBukuD) {
        totalDenda += if (durasiPinjam > batasPinjamCDanDDalamHari) durasiPinjam * biayaDendaPerHari else 0
    }

    if (!isPinjamBukuA && !isPinjamBukuB && !isPinjamBukuC && !isPinjamBukuD) {
        print("Anda tidak meminjam buku apapun")
    } else {
        print("Total denda: $totalDenda")
    }
}