fun main() {
    print("input kata: ")
    val word = readln()
    val lowercaseWord = word.lowercase()
    var isPangram = true
    val letterArray = ('a'..'z').toList()
    letterArray.forEach {
        if (it !in lowercaseWord) {
            isPangram = false
            return@forEach
        }
    }
    print("is $word pangram: $isPangram")
}