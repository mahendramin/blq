import kotlin.math.abs

fun main() {
    print("jam (format jam hh:mm dalam format 12h): ")
    val clockInString = readln().trim().split(":")
    val hour: Int = if (clockInString[0].first() == '0') clockInString[0].drop(1).toInt() else clockInString[0].toInt()
    val minute: Int = if (clockInString[1].first() == '0') clockInString[1].drop(1).toInt() else clockInString[1].toInt()
    val angle = calculateClockAngle(hour, minute)
    println("Sudut antara jarum-jarum pada pukul ${clockInString[0]}:${clockInString[1]} adalah $angle derajat")
}

fun calculateClockAngle(hour: Int, minute: Int): Double {
    val hourAngle = (hour % 12 + minute / 60.0) * 30 // Setiap jam adalah 30 derajat
    val minuteAngle = minute * 6 // Setiap menit adalah 6 derajat

    var angle = abs(hourAngle - minuteAngle)
    if (angle > 180) {
        angle = 360 - angle
    }
    return angle
}