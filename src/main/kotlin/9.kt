fun main() {
    print("Number: ")
    val num = readln().toInt()
    for (i in 1..num) {
        val multipliedNum = num * i
        print("$multipliedNum ")
    }
}