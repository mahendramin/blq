fun main() {
    val list = listOf(1, 2, 4, 7, 8, 6, 9)
    val sortedList = list.sorted()
    //1,2,4,6,7,8,9
    val n = 4
    val minSumList = sortedList.take(n)
    val maxSumList = sortedList.takeLast(n)
    print("min sum: ${minSumList.sum()}, max sum: ${maxSumList.sum()}")

}