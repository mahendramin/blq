import java.time.Duration
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

fun main() {
    println("Format input: dd MMMM yyyy HH:mm:ss")
    println("contoh: 27 Januari 2019 05:00:00")
    print("Waktu masuk: ")
    val waktuMasuk = readln()
    print("Waktu keluar: ")
    val waktuKeluar = readln()
    print(kalkukasiTarifParkir(waktuMasuk, waktuKeluar))
}

fun kalkukasiTarifParkir(tanggalMasuk: String, tanggalKeluar: String): String {
    val localeIndo = Locale("id", "ID")
    val dateTimeFormatter = DateTimeFormatter.ofPattern("dd MMMM yyyy HH:mm:ss", localeIndo)

    val timeTanggalMasuk = LocalDateTime.parse(tanggalMasuk, dateTimeFormatter)
    val timeTanggalKeluar = LocalDateTime.parse(tanggalKeluar, dateTimeFormatter)

    val duration = Duration.between(timeTanggalMasuk, timeTanggalKeluar)
    val selisihJam = duration.toHours()

    val tarif = if (selisihJam > 24) {
        15000 + (selisihJam.toInt() - 24) * 1000
    } else if (selisihJam > 8) {
        8000
    } else {
        if (selisihJam <= 0) 1000 else 1000 * selisihJam.toInt()
    }
    return tarif.toString()
}