fun main() {
    print("n: ")
    val n = readln().toInt()
    generatePrime(n)
}

private fun generatePrime(n: Int) {
    val primes: MutableList<Int> = ArrayList()
    primes.add(2)
    var num = 3
    while (primes.size < n) {
        var isPrime = true
        for (i in primes.indices) {
            if (num % primes[i] == 0) {
                isPrime = false
                break
            }
        }
        if (isPrime) {
            primes.add(num)
        }
        num += 2
    }
    print(primes.joinToString())
}