fun main() {
    print("masukkan input angka, dipisahkan dengan spasi: ")
    //3 9 0 7 1 2 4
    val inputNum = readln()
        .trim()
        .split(" ")
        .toTypedArray()
    val arrayNum = Array(inputNum.size) { 0 }
    for (i in arrayNum.indices) {
        arrayNum[i] = inputNum[i].toInt()
    }

    print("jumlah rotasi: ")
    val totalRotasi = readln().toInt()

    println("nilai awal: ${arrayNum.joinToString()}")

    for (i in 0 until totalRotasi) {
        val firstArrayValue = arrayNum[0]
        val lastArrayValue = arrayNum[arrayNum.size - 1]
        for (j in arrayNum.indices) {
            if (j != arrayNum.lastIndex && j != arrayNum.lastIndex - 1) {
                arrayNum[j] = arrayNum[j + 1]
            } else if (j == arrayNum.lastIndex - 1) {
                arrayNum[j] = lastArrayValue
            } else {
                arrayNum[j] = firstArrayValue
            }
        }
    }
    println("nilai akhir: ${arrayNum.joinToString()}")
}